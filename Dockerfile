FROM mysql:latest

COPY ./etc/mysql/my.cnf /etc/mysql/
COPY ./usr/my.cnf /usr/

CMD ["mysqld"]
